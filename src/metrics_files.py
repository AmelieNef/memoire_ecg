import math

import numpy as np
from sklearn.metrics import multilabel_confusion_matrix, roc_curve, auc, classification_report, confusion_matrix
import src.PARAMETERS as hp
import matplotlib
from mlxtend.evaluate import confusion_matrix as conf_mat
from math import *

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from itertools import cycle
from pathlib import *
import warnings

warnings.filterwarnings('always')
import seaborn as sn
import pandas as pd
from datetime import *


def plot_roc_curve(fpr, tpr, roc_auc):
    """
    Function to plot ROC curve and save parameter to make it
    Parameters
    ----------
    fpr : FPR
    tpr : TPR
    roc_auc : ROC AUC

    Returns
    -------
    /
    """

    timestamp = datetime.today().strftime('%Y%m%d%H%M%S')

    # save all data to reformate it
    fpr_save = pd.DataFrame([fpr[key] for key in fpr.keys()], index=hp.DIAGNOSTIC)
    fpr_save.to_csv(Path.cwd().parent.parent.joinpath('data', 'external', f'fpr_roc_curve_data-{timestamp}.csv'))

    tpr_save = pd.DataFrame([tpr[key] for key in tpr.keys()], index=hp.DIAGNOSTIC)
    tpr_save.to_csv(Path.cwd().parent.parent.joinpath('data', 'external', f'tpr_roc_curve_data-{timestamp}.csv'))

    auc_save = pd.DataFrame([roc_auc[i] for i in roc_auc.keys()], index=hp.DIAGNOSTIC)
    auc_save.to_csv(Path.cwd().parent.parent.joinpath('data', 'external', f'auc_roc_curve_data-{timestamp}.csv'))

    # First aggregate all false positive rates
    all_fpr = np.unique(np.concatenate([fpr[diagnostic] for diagnostic in hp.DIAGNOSTIC]))

    # Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for diagnostic in hp.DIAGNOSTIC:
        mean_tpr += np.interp(all_fpr, fpr[diagnostic], tpr[diagnostic])

    # Plot all ROC curves
    plt.figure()
    colors = cycle(['aqua', 'darkorange', 'cornflowerblue', 'olive', 'navy', 'darksalmon', 'hotpink', 'gold', 'red'])
    for i, color in zip(range(len(hp.DIAGNOSTIC)), colors):
        print(roc_auc)
        plt.plot(fpr[hp.DIAGNOSTIC[i]], tpr[hp.DIAGNOSTIC[i]], color=color, lw=2,
                 label=f'ROC curve of class {hp.DIAGNOSTIC[i]} (area = {roc_auc[hp.DIAGNOSTIC[i]]})')

    plt.plot([0, 1], [0, 1], 'k--', lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Some extension of Receiver operating characteristic to multi-class')
    plt.legend(loc="lower right")
    timestamp = datetime.today().strftime('%Y%m%d%H%M%S')
    directory = Path.cwd().parent.parent.joinpath('picture', f'mygraph{timestamp}.png')
    plt.savefig(directory)
    plt.show()


def compute_distance(f, p):
    """
    Function to compute point to righ high corner distance
    Parameters
    ----------
    f : FPR value
    p : TPR value

    Returns
    -------
    distance
    """
    distance = sqrt((f - 0) ** 2 + (p - 1) ** 2)
    return distance


def threshold_obtention(fpr, tpr, threshold_list):
    """
    Function to compute adaptatif threshold
    Parameters
    ----------
    fpr : FPR
    tpr : TPR
    threshold_list : List of threshold relative of each point

    Returns
    -------
    /
    """
    threshold = dict()
    print(fpr)
    print(tpr)
    print(threshold_list)
    for diagnostic in hp.DIAGNOSTIC:
        max_distance = inf
        for i in range(len(fpr[diagnostic])):
            if compute_distance(fpr[diagnostic][i], tpr[diagnostic][i]) < max_distance:
                max_distance = compute_distance(fpr[diagnostic][i], tpr[diagnostic][i])
                threshold[diagnostic] = threshold_list[diagnostic][i]
    return threshold


def plot_confusion(cm, option, cm_total, label):
    """
    Function to plot confusion matrix
    Parameters
    ----------
    cm : confuson matrix
    option : if multi class or multi label classification
    cm_total : global confusion matrix
    label : label to obtain

    Returns
    -------
    /
    """
    timestamp = datetime.today().strftime('%Y%m%d%H%M%S')

    list_dia = 0
    for i in range(len(hp.DIAGNOSTIC)):
        if hp.DIAGNOSTIC[i] in label:
            if list_dia == 0:
                list_dia = [hp.DIAGNOSTIC[i]]
            else:
                list_dia.append(hp.DIAGNOSTIC[i])

    if list_dia == 0:
        list_dia = hp.DIAGNOSTIC
        cm = [[0 for i in range(len(hp.DIAGNOSTIC))] for j in range(len(hp.DIAGNOSTIC))]

    df_cm = pd.DataFrame(cm, index=list_dia,
                         columns=list_dia)
    directory = Path.cwd().parent.parent.joinpath('data', 'external', f'multiple_confusion_matrix_data-{timestamp}.csv')
    df_cm.to_csv(directory)
    sn.heatmap(df_cm, annot=True)
    plt.xlabel("Predicted")
    plt.ylabel("Reference")
    plt.title("Confusion matrix")
    directory = Path.cwd().parent.parent.joinpath('picture', f'confusion-{timestamp}.png')
    plt.savefig(directory)
    plt.show()

    df_cm = pd.DataFrame(cm_total, index=['Negative', 'Positive'],
                         columns=['Negative', 'Positive'])
    directory = Path.cwd().parent.parent.joinpath('data', 'external', f'global_confusion_matrix_data-{timestamp}.csv')
    df_cm.to_csv(directory)
    sn.heatmap(df_cm, annot=True)
    plt.xlabel("Predicted")
    plt.ylabel("Reference")
    plt.title('Grouped confusion matrix')
    directory = Path.cwd().parent.parent.joinpath('picture', f'global-confusion-{timestamp}.png')
    plt.savefig(directory)
    plt.show()


def save_theo_predict_name(theo, pred, name, model_name, restriction):
    """
    Function to save all predict output value
    Parameters
    ----------
    theo : label theorical
    pred : predicted label
    name : patient name
    model_name : model name
    restriction : restriction value

    Returns
    -------
    /
    """
    name_ = [name_test for name_test in name[0]]
    name_theo = [f'{name}_theo' for name in name_]
    name_pred = [f'{name}_pred' for name in name_]

    value = [[] for i in range(len(hp.DIAGNOSTIC))]
    for i in range(len(theo)):
        for j in range(len(hp.DIAGNOSTIC)):
            value[j].append(theo[i][j])
            value[j].append(pred[i][j])

    list_name = []
    for i in range(len(name_theo)):
        list_name.append(name_theo[i])
        list_name.append(name_pred[i])
    result = {'name': list_name, f'{hp.DIAGNOSTIC[0]}': value[0], f'{hp.DIAGNOSTIC[1]}': value[1],
              f'{hp.DIAGNOSTIC[2]}': value[2],
              f'{hp.DIAGNOSTIC[3]}': value[3], f'{hp.DIAGNOSTIC[4]}': value[4], f'{hp.DIAGNOSTIC[5]}': value[5],
              f'{hp.DIAGNOSTIC[6]}': value[6], f'{hp.DIAGNOSTIC[7]}': value[7], f'{hp.DIAGNOSTIC[8]}': value[8]}

    column_name = ['name']
    column_name = column_name + hp.DIAGNOSTIC
    df = pd.DataFrame(result, columns=column_name)
    timestamp = datetime.today().strftime('%Y%m%d%H%M%S')
    directory = Path.cwd().parent.parent.joinpath('data', 'external',
                                                  f'result_{model_name}_restriction_{restriction}_time{timestamp}.csv')
    df.to_csv(directory, index=False, header=True)


def found_index_1(data):
    """
    Function to translate output standardise
    Parameters
    ----------
    data : output value

    Returns
    -------
    diagnose of output value
    """
    diagnostic = []
    j = 0
    while j < len(hp.DIAGNOSTIC):
        if data[j] == 1:
            if len(diagnostic) == 0:
                diagnostic = [hp.DIAGNOSTIC[j]]
            else:
                diagnostic.append(hp.DIAGNOSTIC[j])
        j += 1

    return diagnostic


def actualize_label(list_index, label):
    """
    Function to keep in memory all label found it
    Parameters
    ----------
    list_index : list of index founded
    label : list of label found it

    Returns
    -------
    label list found
    """
    for elem in list_index:
        if elem not in label and len(label) > 0:
            label.append(elem)
        elif elem not in label and len(label) == 0:
            label = [elem]
    return label


def check_value(numerator, denominator):
    """
    Function to check value (avoid error divided by zero)
    Parameters
    ----------
    numerator : numerator value
    denominator : denominator value

    Returns
    -------
    /
    """
    try:
        result = numerator / denominator
    except (ZeroDivisionError, ValueError):
        result = 0.0

    if math.isnan(result):
        result = 0.0

    return result


def sort_label(a, b):
    """
    Function to sort list value
    Parameters
    ----------
    a : label 1
    b : label 2

    Returns
    -------
    label list label normalise
    """
    same = []
    different = []
    for i in range(len(a)):
        if a[i] in b:
            same.append(a[i])
        else:
            different.append(a[i])
    a = same + different
    return a


def metrics_formula(y_test, y_pred, name, model_name, verbose=1):
    """
    Function to apply all metrics on output found it
    Parameters
    ----------
    y_test : value theoritical
    y_pred : predicted value
    name : patient name
    model_name : model name
    verbose : verbose

    Returns
    -------
    AUC score
    """

    # Compute ROC curve and ROC area for each class
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    threshold_possibility = dict()
    for diagnostic in range(len(hp.DIAGNOSTIC)):
        fpr[hp.DIAGNOSTIC[diagnostic]], tpr[hp.DIAGNOSTIC[diagnostic]], threshold_possibility[hp.DIAGNOSTIC[diagnostic]] \
            = roc_curve(y_test[:, diagnostic], y_pred[:, diagnostic])

        if auc(fpr[hp.DIAGNOSTIC[diagnostic]], tpr[hp.DIAGNOSTIC[diagnostic]]) > 0.0:
            roc_auc[hp.DIAGNOSTIC[diagnostic]] = auc(fpr[hp.DIAGNOSTIC[diagnostic]], tpr[hp.DIAGNOSTIC[diagnostic]])
        else:
            roc_auc[hp.DIAGNOSTIC[diagnostic]] = 0.0

    threshold = threshold_obtention(fpr, tpr, threshold_possibility)
    # threshold = 0.5
    plot_roc_curve(fpr, tpr, roc_auc)

    print("-" * 93)
    # option = True
    option = False
    save_theo_predict_name(y_test, y_pred, name, model_name, option)

    if not option:
        print(f"Threshold: {threshold}")
        print('pred init', len(y_pred))
        print("reverse", sorted(y_pred[0], reverse=True)[:sum(y_test[0])])
        y_pred_int = [
            [1 if y_pred[index][i] >= threshold[hp.DIAGNOSTIC[i]] and y_pred[index][i] in sorted([y_pred[index][j]
                                                                                                  for j in range(
                    len(y_pred[index])) if y_pred[index][j] >= threshold[hp.DIAGNOSTIC[i]]], reverse=True)[
                                                                                          :sum(y_test[index])] else 0
             for i
             in range(len(y_pred[index]))] for index in range(len(y_pred))]
        y_pred_int = np.reshape(y_pred_int, (len(y_pred_int), len(hp.DIAGNOSTIC)))

    else:
        print(sorted([max(y_) for y_ in y_pred]))
        print('min', min([max(y_) for y_ in y_pred]))
        print('mean', np.mean([max(y_) for y_ in y_pred]))
        print('median', np.median([max(y_) for y_ in y_pred]))
        print(f"Threshold: {threshold}")
        print('pred init', len(y_pred))
        y_pred_int = [[1 if y_[i] >= threshold[hp.DIAGNOSTIC[i]] and y_[i] == max(y_) else 0 for i in range(len(y_))]
                      for y_ in y_pred]
        # y_pred_int = [[1 if y_[i] >= 0.5 and y_[i] == max(y_) else 0 for i in range(len(y_))] for y_ in y_pred]
        y_pred_int = np.reshape(y_pred_int, (len(y_pred_int), len(hp.DIAGNOSTIC)))

    normalize_label_theo = []
    normalize_label_prac = []
    label = []

    for i in range(len(y_test)):
        print(y_test[i])
        print(y_pred_int[i])
        print(found_index_1(y_test[i]))
        print(found_index_1(y_pred_int[i]))
        if len(found_index_1(y_test[i])) == len(found_index_1(y_pred_int[i])):
            a = found_index_1(y_test[i])
            label = actualize_label(found_index_1(y_test[i]), label)
            b = found_index_1(y_pred_int[i])
            label = actualize_label(found_index_1(y_pred_int[i]), label)

            a = sort_label(a, b)
            b = sort_label(b, a)
            for j in range(len(a)):
                normalize_label_theo.append(a[j])
                normalize_label_prac.append(b[j])

    print("all label found : ", label)
    cm_multilabel = conf_mat(y_target=normalize_label_theo, y_predicted=normalize_label_prac, binary=False)
    print(cm_multilabel)

    cm_total = confusion_matrix(y_test.flatten(), y_pred_int.flatten())
    plot_confusion(cm_multilabel, option, cm_total, label)
    tn_total = cm_total[0][0]
    fn_total = cm_total[1][0]
    tp_total = cm_total[1][1]
    fp_total = cm_total[0][1]
    total_all = np.sum(fp_total + fn_total + tp_total + tn_total)

    precision = check_value(tp_total, (tp_total + fp_total))
    recall = check_value(tp_total, (tp_total + fn_total))
    try:
        f1 = check_value(2 * (recall * precision), recall + precision)
    except (ZeroDivisionError, ValueError):
        f1 = 0.0

    accuracy = check_value((tp_total + tn_total), total_all)
    sensitivity = check_value(tp_total, (tp_total + fn_total))
    specificity = check_value(tn_total, (tn_total + fp_total))
    ppv = check_value(tp_total, (tp_total + fp_total))
    npv = check_value(tn_total, (tn_total + fn_total))
    false_positive = check_value(fp_total, total_all)
    false_negative = check_value(fn_total, total_all)

    if verbose:
        print("-" * 70)
        print(f'AUC:               {roc_auc}')
        print(f"F1:                {f1}")
        print(f"Precision:          {precision}")
        print(f"Recall:              {recall}")
        print("-" * 35)
        print(f"Accuracy:         {accuracy}")
        print(f"Sensitivity:        {sensitivity}")
        print(f"Specificity:           {specificity}")
        print(f"PPV:             {ppv}")
        print(f"NPV:               {npv}")
        print(f"False positive:          {false_positive}")
        print(f"False negative:          {false_negative}")
        print("i" * 70)
        print("Confusion Matrix \n")
        print(cm_multilabel)
        print("Summary")
        print(classification_report(y_test, y_pred_int, zero_division=0))

    return roc_auc
