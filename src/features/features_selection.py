from pathlib import *
import json
import random
import numpy as np
import src.PARAMETERS as hp


def translate_desease(label):
    """
    Function in order to create label vector due of each patient
    Parameters
    ----------
    label : diagnostic name

    Returns
    -------
    specific vector of this diagnostic
    """
    all_new = []
    for i in range(len(label)):
        new = []
        for j in range(len(label[i])):
            for diagnostic in hp.DIAGNOSTIC:
                if diagnostic in label[i][j]:
                    new.append(1)
                else:
                    new.append(0)

        all_new.append(new)

    return all_new


def complet_missing_value(data, raw):
    """
    Function to normalize lenght of data
    Parameters
    ----------
    data : raw data
    raw : window size

    Returns
    -------
    raw data normalize
    """
    for layers in range(len(data)):
        if len(data[layers]) < raw:
            while len(data[layers]) < raw:
                data[layers].append(-1)
        elif len(data[layers]) > raw:
            data[layers] = data[layers][:raw]
    return data


def splitter_set(threshold, data, label, index, name, raw):
    """
    Function to create list of data and list of label patient
    Parameters
    ----------
    threshold : limit lenght of created list
    data : raw data
    label : list of label patient
    index : list of index patient name list not yet taking in a set
    name : patient name
    raw : window size

    Returns
    -------
    data set, label set, list of index name patient not yet using updating, list of patient name choosing to create set
    """
    x = []
    y = []
    label_choosing = []

    while len(index) > threshold:
        select = random.randint(0, len(index) - 1)
        x_adding = complet_missing_value(data[select], raw)
        x.append(x_adding)
        y.append([label[select]])

        label_choosing.append(name[index[select]])

        del index[select]

    x = np.reshape(x, (len(x), hp.NUMBER_OF_LAYERS, raw))
    y = translate_desease(y)

    y = np.asarray(y)

    return x, y, index, label_choosing


def split_dataset(data, label, purcent_validation, purcent_test, list_name, raw):
    """
    Function to create all subdataset - test, train and validation
    Parameters
    ----------
    data : raw data
    label : label of data
    purcent_validation : fraction to patient need in validation set
    purcent_test : fraction to patient need in test set
    list_name : list of patient name
    raw : window size

    Returns
    -------
    All subset dataset and sublabel set relative
    """

    data_index = [i for i in range(len(label))]
    data_index = random.sample(data_index, len(data_index))
    x_test, y_test, data_index, index_test = splitter_set(len(data_index) - purcent_test * len(data_index), data, label,
                                                          data_index, list_name, raw)
    x_validation, y_validation, data_index, index_validation = splitter_set(int(len(data_index) -
                                                                                purcent_validation * len(data_index)),
                                                                            data, label, data_index, list_name, raw)
    x_train = [complet_missing_value(data[index], raw) for index in data_index]

    x_train = np.reshape(x_train, (len(x_train), 12, raw))
    y_train = [[label[index]] for index in data_index]
    y_train = translate_desease(y_train)
    y_train = np.reshape(y_train, (len(y_train), len(hp.DIAGNOSTIC)))

    data_name = [list_name[data_index[i]] for i in range(len(data_index))]

    dico = {}
    dico["test"] = index_test
    dico["validation"] = index_validation
    dico["train"] = data_name

    directory = Path.cwd().parent.parent.joinpath('data', 'processed', 'reference_split.txt')
    json.dump(dico, open(directory, "w"))

    name = [index_test, index_validation, data_name]

    return x_train, y_train, x_validation, y_validation, x_test, y_test, name


def recuperation_information():
    """
    Function to get back information file
    Returns
    -------
    Dictionnary from information file
    """
    path = Path.cwd().parent.parent.joinpath('data', 'processed', 'informations.txt')
    with open(path, 'r') as fp:
        data = json.load(fp)
    return data


def recuperation_features():
    """
    Function to get back features data
    Returns
    -------
    data and label, with patient name and split of this patient in test, train and validation set
    """
    path = Path.cwd().parent.parent.joinpath('data', 'processed')
    list_name_txt = [f.name for f in path.rglob('*_interval_*')]
    list_split_txt = [f.name for f in path.rglob('reference_split.txt')]

    information = recuperation_information()

    x = []
    y = []
    compteur = 0

    for name_path in list_name_txt:

        # QRS interval
        # name = name_path[:-17]

        # Other interval
        name = name_path[:-16]

        path_2 = Path.cwd().parent.parent.joinpath('data', 'processed', name_path)
        with open(path_2, 'r') as fp:
            data = json.load(fp)

        x_data = []
        for keys in data.keys():
            if len(data[keys]) == 0:
                x_data.append([-1])
            else:
                x_data.append(data[keys])

        x.append(x_data)
        y.append(information[name]["Type"])

        compteur += 1

    if len(list_split_txt) != 0:
        path_2 = Path.cwd().parent.parent.joinpath('data', 'processed', list_split_txt[0])
        with open(path_2, 'r') as fp:
            data = json.load(fp)
        splitter = [data[keys] for keys in data.keys()]
        list_split_txt = splitter

    # QRS interval
    # list_name_txt = [name[:-17] for name in list_name_txt]

    # Other interval
    list_name_txt = [name[:-16] for name in list_name_txt]

    return x, y, list_name_txt, list_split_txt


def obtain_set(data, label, name, split, raw):
    """
    Function to recreate subset
    Parameters
    ----------
    data : raw data
    label : list name files
    name : patient name
    split : list of name patient in this subset
    raw : window size

    Returns
    -------
    data and label of subset
    """
    x = []
    y = []
    if len(name[0]) < len(split[0]):
        split = [split[i] for i in range(len(split))]

    for name_split in split:
        select = [index for index in range(len(name)) if name[index] == name_split]
        x_adding = complet_missing_value(data[select[0]], raw)
        x.append(x_adding)
        y.append([label[select[0]]])

    x = np.reshape(x, (len(x), hp.NUMBER_OF_LAYERS, raw))
    y = translate_desease(y)

    y = np.asarray(y)
    return x, y


def retrive_split_dataset(data, label, name, split, raw):
    """
    Function to reacreate test, validation and train set
    Parameters
    ----------
    data : raw data
    label : label of all patient data
    name : list of name files
    split : matrix with each row equal of list patient in a specific subset
    raw : window size

    Returns
    -------
    all subsetdata and label
    """
    x_test, y_test = obtain_set(data, label, name, split[0], raw)
    x_validation, y_validation = obtain_set(data, label, name, split[1], raw)
    x_train, y_train = obtain_set(data, label, name, split[2], raw)

    return x_train, y_train, x_validation, y_validation, x_test, y_test


def feature_select(raw):
    """
    Function to take back information data and label of dataset and recreate them
    Parameters
    ----------
    raw : window size

    Returns
    -------
    All subset data and label and matrix relative of split to apply to make subdataset and original data
    """
    x, y, list_name, split = recuperation_features()
    if len(split) == 0:
        x_train, y_train, x_validation, y_validation, x_test, y_test, split = split_dataset(x, y, 0.2, 0.2, list_name,
                                                                                            raw)
    else:
        x_train, y_train, x_validation, y_validation, x_test, y_test = retrive_split_dataset(x, y, list_name, split,
                                                                                             raw)
    return x_train, y_train, x_validation, y_validation, x_test, y_test, split, x, y
