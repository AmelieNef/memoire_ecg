from src.data.make_dataset import *
from src.data.preprocessing import *
import json

#Wave detection
def compute_hauteur(subdata):
    """
    Function to compute height of wave
    Parameters
    ----------
    subdata : data of layer from one patient

    Returns
    -------
    list of heigh spike and localisation of spike in subdata (index)
    """
    peaks, property = find_peaks(subdata)
    prominences = peak_prominences(subdata, peaks)[0]
    contour_heights = np.array(subdata)[peaks] - prominences
    hauteur = []

    for i in range(len(prominences)):
        hauteur.append(np.array(subdata)[peaks][i] - contour_heights[i])

    return hauteur,peaks

def detect_pic(subdata, info, name, layers_name):
    """
    Found each pic wich could be are PQRS.
    Parameters
    ----------
    subdata : data of one layer from a patient
    info : information about patient
    name : patient name
    layers_name : layers of patient that we inspect

    Returns
    -------
    index of spike and his Y value (mV)
    """

    peaks_ = []
    data_Y = []
    hauteur_final = []

    hauteur, peaks = compute_hauteur(subdata)

    #Preparation en absence de préprocessing
    """
    for j in range(len(hauteur)):
        if hauteur[j] > np.percentile(hauteur, 80, interpolation='midpoint') and \
                (len(peaks_) == 0 or peaks[j] - peaks_[-1] > 20 or abs(hauteur_final[-1] - hauteur[j]) > 30) \
                and (len(peaks_) == 0 or abs(subdata[peaks[j]] - subdata[peaks_[-1]]) > 30 or peaks[j] - peaks_[-1] > 20):

            hauteur_final.append(hauteur[j])
            peaks_.append(peaks[j])
            data_Y.append(np.array(subdata)[peaks][j])

        elif hauteur[j] > np.percentile(hauteur, 80, interpolation='midpoint') and \
                len(peaks_) != 0 and \
                (peaks[j] - peaks_[-1] < 30) and \
                (hauteur[j] > hauteur_final[-1]) and \
                0 < abs(hauteur[j] - hauteur_final[-1]) < max(hauteur)/2:

            hauteur_final[-1] = hauteur[j]
            peaks_[-1] = peaks[j]
            data_Y[-1] = np.array(subdata)[peaks][j]
    """

    for j in range(len(hauteur)):
        hauteur_final.append(hauteur[j])
        peaks_.append(peaks[j])
        data_Y.append(np.array(subdata)[peaks][j])

    #Obtention_ECG(subdata, peaks_, data_Y, info[name]["Amplitude"], layers_name)

    return peaks_, data_Y


def found_Pic(data, pic, index_pic):
    """
    Function to found index of a spike in raw data
    Parameters
    ----------
    data : raw data
    pic : value Y of spike
    index_pic : index of spike in value Y

    Returns
    -------
    spike index in raw data
    """
    init_data = 0
    # Trouvé l'indice du pic R actuel
    for data_index in range(init_data, len(data)):
        if data[data_index] == pic[index_pic]:
            init_data = data_index
            break

    return init_data

#Onde P
def detect_pic_P(data, hauteur, subdata, info, name, layers_name, R_spike, hauteurRSpike):
    """
    Function to found P spike
    Parameters
    ----------
    data : raw data
    hauteur : heigh spike
    subdata : data of one layer to a patient
    info : information about patient
    name : patient name
    layers_name : layer name
    R_spike : list index R spike on raw data
    hauteurRSpike : Height spike R list

    Returns
    -------
    Index P spike
    """
    dataP = []
    hauteurP = []
    for index_pic_R in range(len(R_spike)):

        # Trouvé l'indice du pic R actuel
        init_data = found_Pic(data, R_spike, index_pic_R)

        # Trouver l'indice du pic R précédant
        if index_pic_R > 0:
            for previous_data_index in range(0, init_data):
                if R_spike[index_pic_R - 1] == data[previous_data_index]:
                    previous_data = previous_data_index
                    break
        else:
            previous_data = 0

        if init_data > 1:
            if previous_data == 0:
                extremity = 0
            else:
                extremity = init_data - ((init_data - previous_data) // 3)

            best_index = extremity
            for i in range(extremity+1, init_data):
                if hauteur[i] > hauteur[best_index] and hauteur[i] < (np.mean(hauteurRSpike)//2):
                    best_index = i

            dataP.append(data[best_index])
            hauteurP.append(hauteur[best_index])

    #Obtention_ECG(subdata, dataP, hauteurP, info[name]["Amplitude"], layers_name)

    return dataP
#Onde Q
def detect_pic_Q(data, hauteur, subdata, info, name, layers_name, R_spike, P_spike):
    """
    Function to found Q spike
    Parameters
    ----------
    data : raw data
    hauteur : heigh spike
    subdata : data of one layer to a patient
    info : information about patient
    name : patient name
    layers_name : layer name
    R_spike : list index R spike on raw data
    P_spike : list index P spike on raw data

    Returns
    -------
    Index Q spike
    """
    dataQ = []
    hauteurQ = []
    for index_pic in range(min(len(R_spike), len(P_spike))):
        # Trouvé l'indice du pic R et Q
        index_R = R_spike[index_pic]
        index_P = P_spike[index_pic]
        low_search_index = index_P+1
        for index_search in range(index_P+2, index_R):
            if subdata[index_search] < subdata[low_search_index]:
                low_search_index = index_search
        dataQ.append(low_search_index)
        hauteurQ.append(subdata[low_search_index])

    #Obtention_ECG(subdata, dataQ, hauteurQ, info[name]["Amplitude"], layers_name)
    return dataQ

#Onde T
def detect_pic_T(data, hauteur, subdata, info, name, layers_name, R_spike, hauteurRSpike):
    """
    Function to found T spike
    Parameters
    ----------
    data : raw data
    hauteur : heigh spike
    subdata : data of one layer to a patient
    info : information about patient
    name : patient name
    layers_name : layer name
    R_spike : list index R spike on raw data
    hauteurRSpike : Height spike R list

    Returns
    -------
    Index T spike
    """
    dataT = []
    hauteurT = []
    for index_pic_R in range(len(R_spike)):

        # Trouvé l'indice du pic R actuel
        init_data = found_Pic(data, R_spike, index_pic_R)

        # Trouver l'indice du pic R suivant
        next_data = len(data)
        if index_pic_R < len(R_spike) - 1:
            for next_data_index in range(init_data+1, len(data)):
                if R_spike[index_pic_R + 1] == data[next_data_index]:
                    next_data = next_data_index
                    break

        best_index = init_data + 1
        if best_index < len(data):
            if next_data == len(data):
                extremity = len(data)
            else:
                extremity = init_data + (next_data-init_data)//3

            for i in range(init_data + 2, extremity):
                if i >= len(data):
                    break
                if hauteur[i] > hauteur[best_index] and hauteur[i] < (np.mean(hauteurRSpike)//2):
                    best_index = i

            dataT.append(data[best_index])
            hauteurT.append(hauteur[best_index])

    #Obtention_ECG(subdata, dataT, hauteurT, info[name]["Amplitude"], layers_name)

    return dataT

#Onde S
def detect_pic_S(data, hauteur, subdata, info, name, layers_name, R_spike, T_spike):
    """
    Function to found S spike
    Parameters
    ----------
    data : raw data
    hauteur : heigh spike
    subdata : data of one layer to a patient
    info : information about patient
    name : patient name
    layers_name : layer name
    R_spike : list index R spike on raw data
    T_spike : list index T spike on raw data

    Returns
    -------
    Index S spike
    """
    dataS = []
    hauteurS = []
    for index_pic in range(min(len(R_spike), len(T_spike))):
        # Trouvé l'indice du pic R et Q
        index_T = T_spike[index_pic]
        index_R = R_spike[index_pic]

        low_search_index = index_R+1
        for index_search in range(index_R+2, index_T):
            if subdata[index_search] < subdata[low_search_index]:
                low_search_index = index_search
        dataS.append(low_search_index)
        hauteurS.append(subdata[low_search_index])

    #Obtention_ECG(subdata, dataS, hauteurS, info[name]["Amplitude"], layers_name)
    return dataS


#detecte pic R
def filtre_1(data, hauteur, taille):
    """
    Function to remove P and T spike and found R spike
    Parameters
    ----------
    data : raw data
    hauteur : height spike
    taille : min of spike to detect

    Returns
    -------
    Index of R spike and his height
    """
    dataR = []
    hauteurR = []
    decalage = 0
    while True:
        for i in range(decalage,len(hauteur)):
            if max(hauteur)/2 <= hauteur[i]:
                dataR.append(data[i])
                hauteurR.append(hauteur[i])
        if len(dataR) < taille and decalage+10 < len(hauteur):
            decalage += 10
        else:
            break
    return dataR, hauteurR

def detect_pic_R(data, hauteur, subdata, info, name, layers_name, taille):
    """
    Function to make sure that we extract only R spike
    Parameters
    ----------
    data : raw data
    hauteur : heigh spike
    subdata : data of a layer to one patient
    info : information about a patient
    name : patient name
    layers_name : layer explore
    taille : lenght minimum of spike list detect

    Returns
    -------
    R spike index list and them heights
    """
    dataR, hauteurR = filtre_1(data,hauteur,taille)

    #Needed only if we didn't apply some preprocessing step
    """
    dataR_filter = []
    if len(dataR) != 0:
        #Permet d'éviter toutes discorcandance et confusion entre le pic R et le pic T qui peut parfois être collé.
        threshold_verification = ((max(dataR) - min(dataR)) / len(dataR)) - ((max(dataR) - min(dataR)) / len(dataR)) * 0.05
        hauteurR_filter = []
        for i in range(len(dataR)):
            if len(dataR_filter) == 0 or dataR[i]-dataR_filter[-1] > threshold_verification:
                dataR_filter.append(dataR[i])
                hauteurR_filter.append(hauteurR[i])
        #print("RR")
        #Obtention_ECG(subdata, dataR, hauteurR, info[name]["Amplitude"], layers_name)
        #Obtention_ECG(subdata, dataR_filter, hauteurR_filter, info[name]["Amplitude"], layers_name)

    return dataR_filter, len(dataR_filter), 0
    #"""

    #Obtention_ECG(subdata, dataR, hauteurR, info[name]["Amplitude"], layers_name)

    return dataR, len(hauteurR), hauteurR

#Interval de temps
def compute_interval(info, data_previous, data_next, name):
    """
    Function to compute interval between two spike different type
    Parameters
    ----------
    info : informations about patient
    data_previous : data of one type spike
    data_next : data of other type spike
    name : patient name

    Returns
    -------
    list of interval normalise
    """
    interval = []
    for i in range(len(data_previous)):
        if i == len(data_next):
            break

        interval_brut = data_next[i] - data_previous[i]
        interval_normalise = abs(interval_brut / int(info[name]["Amplitude"]))
        interval.append(interval_normalise)

    return interval

def compute_interval_samespike(info, data, name):
    """
    Function to obtain interval between two spike of same type
    Parameters
    ----------
    info : information about patient
    data : raw data
    name : patient name

    Returns
    -------
    interval of spike similar
    """

    interval = []
    for i in range(1,len(data)):
        interval_brut = data[i] - data[i-1]
        interval_normalise = abs(interval_brut / int(info[name]["Amplitude"]))
        interval.append(interval_normalise)
    return interval

#TYPE
def detect_interval_RR(info, name_file):
    """
    Function to produce RR files features to each patient
    Parameters
    ----------
    info : information about patient
    name_file : list of patient

    Returns
    -------
    /
    """
    RR_interval = {}
    i = 0
    for name in name_file:
        print(len(name_file)-i)
        RR_interval[name] = {}
        taille = 5
        #Obtention_ECG_all_leads(name_file[name], name)
        for layers_name in name_file[name]:
            subdata = name_file[name][layers_name]
            data, hauteur = detect_pic(subdata, info, name, layers_name)
            dataR, taille,_ = detect_pic_R(data,hauteur, subdata, info, name, layers_name, taille)
            RR_interval[name][layers_name] = compute_interval_samespike(info, dataR, name)
        i += 1

        directory = Path.cwd().parent.parent.joinpath('data', 'processed', f'{name}_interval_RR.txt')
        json.dump(RR_interval[name], open(directory, "w"))

#Interval PR
def detect_interval_PR(info, name_file):
    """
    Function to produce PR files features to each patient
    Parameters
    ----------
    info : information about patient
    name_file : list of patient

    Returns
    -------
    /
    """
    PR_interval = {}
    i = 0
    for name in name_file:
        print(len(name_file)-i)
        PR_interval[name] = {}
        taille = 5
        for layers_name in name_file[name]:
            subdata = name_file[name][layers_name]
            data, hauteur = detect_pic(subdata, info, name, layers_name)
            dataR, taille, hauteurR = detect_pic_R(data,hauteur, subdata, info, name, layers_name, taille)
            picP = detect_pic_P(data, hauteur, subdata, info, name, layers_name, dataR, hauteurR)
            PR_interval[name][layers_name] = compute_interval(info, picP, dataR, name)
        i += 1

        directory = Path.cwd().parent.parent.joinpath('data', 'processed', f'{name}_interval_PR.txt')
        json.dump(PR_interval[name], open(directory, "w"))

def detect_interval_PP(info, name_file):
    """
    Function to produce PP files features to each patient
    Parameters
    ----------
    info : information about patient
    name_file : list of patient

    Returns
    -------
    /
    """
    PP_interval = {}
    i = 0
    for name in name_file:
        print(len(name_file)-i)
        PP_interval[name] = {}
        taille = 5
        for layers_name in name_file[name]:
            subdata = name_file[name][layers_name]
            data, hauteur = detect_pic(subdata, info, name, layers_name)
            dataR, taille, hauteurR = detect_pic_R(data,hauteur, subdata, info, name, layers_name, taille)
            picP = detect_pic_P(data, hauteur, subdata, info, name, layers_name, dataR, hauteurR)
            PP_interval[name][layers_name] = compute_interval_samespike(info, picP, name)
        i += 1

        directory = Path.cwd().parent.parent.joinpath('data', 'processed', f'{name}_interval_PP.txt')
        json.dump(PP_interval[name], open(directory, "w"))


#Interval QT
def detect_interval_QT(info, name_file):
    """
    Function to produce QT files features to each patient
    Parameters
    ----------
    info : information about patient
    name_file : list of patient

    Returns
    -------
    /
    """
    print("intervalQT")
    QT_interval = {}
    i = 0
    for name in name_file:
        print(len(name_file)-i)
        QT_interval[name] = {}
        taille = 5
        for layers_name in name_file[name]:
            subdata = name_file[name][layers_name]
            data, hauteur = detect_pic(subdata, info, name, layers_name)
            dataR, taille, hauteurR = detect_pic_R(data,hauteur, subdata, info, name, layers_name, taille)
            picP = detect_pic_P(data, hauteur, subdata, info, name, layers_name, dataR, hauteurR)
            picQ = detect_pic_Q(data, hauteur, subdata, info, name, layers_name, dataR, picP)
            picT = detect_pic_T(data, hauteur, subdata, info, name, layers_name, dataR, hauteurR)
            QT_interval[name][layers_name] = compute_interval(info, picQ, picT, name)
        i += 1

        directory = Path.cwd().parent.parent.joinpath('data', 'processed', f'{name}_interval_QT.txt')
        json.dump(QT_interval[name], open(directory, "w"))


#Interval QRS
def detect_interval_QRS(info, name_file):
    """
    Function to produce QRS files features to each patient
    Parameters
    ----------
    info : information about patient
    name_file : list of patient

    Returns
    -------
    /
    """
    QRS_interval = {}
    i = 0
    for name in name_file:
        print(len(name_file)-i)
        QRS_interval[name] = {}
        taille = 5
        for layers_name in name_file[name]:
            subdata = name_file[name][layers_name]
            data, hauteur = detect_pic(subdata, info, name, layers_name)
            dataR, taille, hauteurR = detect_pic_R(data,hauteur, subdata, info, name, layers_name, taille)
            picP = detect_pic_P(data, hauteur, subdata, info, name, layers_name, dataR, hauteurR)
            picQ = detect_pic_Q(data, hauteur, subdata, info, name, layers_name, dataR, picP)
            picT = detect_pic_T(data, hauteur, subdata, info, name, layers_name, dataR, hauteurR)
            picS = detect_pic_S(data, hauteur, subdata, info, name, layers_name, dataR, picT)
            QRS_interval[name][layers_name] = compute_interval(info, picQ, picS, name)
        i += 1

        directory = Path.cwd().parent.parent.joinpath('data', 'processed', f'{name}_interval_QRS.txt')
        json.dump(QRS_interval[name], open(directory, "w"))

#Interval ST
def detect_interval_ST(info, name_file):
    """
    Function to produce ST files features to each patient
    Parameters
    ----------
    info : information about patient
    name_file : list of patient

    Returns
    -------
    /
    """
    ST_interval = {}
    i = 0
    for name in name_file:
        print(len(name_file)-i)
        ST_interval[name] = {}
        taille = 5
        for layers_name in name_file[name]:
            subdata = name_file[name][layers_name]
            data, hauteur = detect_pic(subdata, info, name, layers_name)
            dataR, taille, hauteurR = detect_pic_R(data,hauteur, subdata, info, name, layers_name, taille)
            picT = detect_pic_T(data, hauteur, subdata, info, name, layers_name, dataR, hauteurR)
            picS = detect_pic_S(data, hauteur, subdata, info, name, layers_name, dataR, picT)
            ST_interval[name][layers_name] = compute_interval(info, picS, picT, name)
        i += 1

        directory = Path.cwd().parent.parent.joinpath('data', 'processed', f'{name}_interval_ST.txt')
        json.dump(ST_interval[name], open(directory, "w"))


#C - Main

def create_features_files(raw):
    """
    Function aim to produce features
    Parameters
    ----------
    raw : window size

    Returns
    -------
    /
    """

    #If we didn't have yet create split set
    #print("1")
    #element, detail = make_dataset()

    #If we have yet create split set
    print("1")
    element, detail, _, _ = sub_build_dataset_raw()

    print("2")
    element = preprocessing_step(element, detail, raw)

    print("3")
    #detect_interval_RR(detail, element)
    #detect_interval_PR(detail, element)
    detect_interval_QT(detail, element)
    #detect_interval_ST(detail, element)
    #detect_interval_QRS(detail, element)
    #detect_interval_PP(detail, element)


create_features_files(hp.TRUE_WINDOW)
