from src.data.make_dataset import *
from src.features.features_selection import *
from src.data.preprocessing import *
from src.data.dataset_extraction import *


def make_set(dico, information_dico):
    """
    Function to obtain Label list relative to Data name - keys of  data dictionnary
    Parameters
    ----------
    dico : data dictionnary
    information_dico : information about each patient

    Returns
    -------
    List of  data and relative label
    """
    x = []
    y = []

    for name in dico.keys():
        x_data = []
        for keys in dico[name].keys():
            if len(dico[name][keys]) == 0:
                x_data.append([-1])
            else:
                x_data.append(dico[name][keys])
        # x_data = data['I']
        x.append(x_data)
        y.append(information_dico[name]["Type"])

    return x, y


def sub_build_dataset_raw():
    """
    Function to extract data and recreacte dataset - dictionnary with three key : test, train and validation
    Returns
    -------
    dictionnary dataset, information relative and list of name to each different subset (test, train and validation),
    name of all raw file.
    """
    information_set = recuperation_information()
    list_file = [name for name in information_set.keys()]
    path = Path.cwd().parent.parent.joinpath('data', 'processed')
    list_split_txt = [f.name for f in path.rglob('reference_split.txt')]

    if len(list_split_txt) != 0:
        path_2 = Path.cwd().parent.parent.joinpath('data', 'processed', list_split_txt[0])
        with open(path_2, 'r') as fp:
            data_ = json.load(fp)

        splitter = [data_[keys] for keys in data_.keys()]
        list_split_txt = splitter

    # Found .mat file relative
    dico_data = data(list_file)

    # CMake all data positive
    dataset = mirror_data(dico_data)
    return dataset, information_set, list_split_txt, list_file


def build_dataset_raw(raw):
    """
    Function to make all raw subset (input model = raw data)
    Parameters
    ----------
    raw : Window size

    Returns
    -------
    All subset set (x: data/ y: label)
    """
    dataset_temoin_check, information_set, list_split_txt, list_file = sub_build_dataset_raw()

    # preprocessing
    dataset_temoin_check = preprocessing_step(dataset_temoin_check, information_set, raw)

    # Preparation of datasets
    x, y = make_set(dataset_temoin_check, information_set)

    if len(list_split_txt) == 0:
        x_train, y_train, x_validation, y_validation, x_test, y_test, list_split_txt = split_dataset(x, y, 0.2, 0.2,
                                                                                                     list_file, raw)

    else:
        x_train, y_train, x_validation, y_validation, x_test, y_test = retrive_split_dataset(x, y, list_file,
                                                                                             list_split_txt, raw)

    return x_train, y_train, x_validation, y_validation, x_test, y_test, list_split_txt
