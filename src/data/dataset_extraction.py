import pandas as pd
from pathlib import *
import scipy.io as sio

#Importation des noms de fichiers contenus dans le dossier
def layers_description():
    """
    Function to obtain all patient information
    Returns
    -------
    List of all raw data name file and dataframe information of patient.
    """
    path = Path.cwd().parent.parent.joinpath('data', 'raw')
    list_name_hea = [f.name for f in path.rglob('*.hea')]
    all_data = []
    all_description = []

    for name in list_name_hea:
        description = []

        arg = Path.cwd().parent.parent.joinpath('data', 'raw', name).open(mode='r')
        first = arg.readline()  # header
        first = first.split(' ')
        for i in range(4):
            description.append(first[i])

        for line in arg:
            data = line.split(" ")
            del data[-2]
            data[-1] = data[-1][:-1]
            if len(data) == 1:
                data = data[0].split(",")
                description.append(data)
            else:
                all_data.append(data)

        all_description.append(description)

    #Useless informations
    Layers = pd.DataFrame(all_data, columns=['File_Name', 'Bits_writing/offset', 'Amplitude', 'Resolution_Bits',
                                              'Baseline_Value','First_Signal_Value','Checksum','Lead_Name'])

    #Usefull informattions
    Description = pd.DataFrame(all_description, columns=['Reference', 'Layers_numbers', 'Sample_frequency', 'Sample_numbers',
                                                      'Age','Sexe','Type','Medical_prescription', 'History', 'Symptom/Surgery'])

    list_name_mat = [f.name for f in path.rglob('*.mat')]

    return list_name_mat, Description

def data(list_name):
    """
    Function to obtain data of ECG
    Parameters
    ----------
    list_name : list of all raw data name file that we want extract data

    Returns
    -------
    Dictionnary with all patient data subdivided in subkey layer
    """
    LIST_LAYERS = ['I', 'II', 'III', 'aVR', 'aVL', 'aVF', 'V1', 'V2', 'V3', 'V4', 'V5', 'V6']
    Data = {}
    iteration = 0
    for name in list_name:
        contenus = sio.loadmat(Path.cwd().parent.parent.joinpath('data', 'raw', name + ".mat"))
        extract = contenus['val']
        data = pd.DataFrame(extract)
        Data[name] = {}
        for i in range(12):
            Data[name][LIST_LAYERS[i]] = list(data.iloc[i,:])

        iteration += 1

    return Data

