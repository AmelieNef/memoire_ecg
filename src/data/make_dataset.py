from src.data.build_dataset_raw import *
import json
import math
from src.data.dataset_extraction import *
import random

def information(set_name, dataframe):
    """
    Function to select only important informations to each information patient
    Parameters
    ----------
    set_name : name file patient
    dataframe : information dataframe

    Returns
    -------
    Dictionnary of patient key with subkey important information
    """
    print("information")
    information = {}
    for name in set_name:
        information[name] = {}
        for i in range(len(dataframe.iloc[:,0])):
            if dataframe.iloc[i,0] == name:
                information[name]["Amplitude"] = dataframe.iloc[i,2]
                information[name]["Resolution_Bits"] = dataframe.iloc[i,3]
                if len(dataframe.iloc[i,6]) == 1:
                    information[name]["Type"] = dataframe.iloc[i,6][0]
                else:
                    type = ""
                    for index in range(len(dataframe.iloc[i,6])):
                        type += dataframe.iloc[i, 6][index]
                        if index != len(dataframe.iloc[i,6])-1:
                            type += ", "
                    information[name]["Type"] = type
                break

    return information

def check(old):
    """
    Function to avoid a patient duplication in next dataset
    Parameters
    ----------
    old : all patient already present in  next dataset

    Returns
    -------
    Old updating
    """
    new = []
    for i in range(len(old)):
        if not old[i] in new:
            new.append(old[i])
    return new

def sample_malade_temoin(dico):
    """
    Function to extract same patient of each subset diagnostic cohorte
    Parameters
    ----------
    dico: dictionnary with patient name relative to each possible diagnose

    Returns
    -------
    Minimum subset cohorte found (lenght of each cohort diagnosis) and list of patient to extract data
    """
    list_patient = []
    minimum = math.inf
    for key in dico.keys():
        print(key, ' ', len(dico[key]))
        if len(dico[key]) < minimum:
            minimum = len(dico[key])
    print('minimum', minimum)
    for key in dico.keys():
        list_patient = list_patient + random.sample(dico[key], k=minimum)
    print('before', len(list_patient))
    list_patient = check(list_patient)
    print('after', len(list_patient))

    return list_patient, minimum

def make_dataset():
    """
    Function to obtain dataset and informations dictionnnary of patient in dataset
    Returns
    -------

    """
    print("dataset")
    # Obtention de la table récapitulative de mes données
    annuaire, detail_of_files = layers_description()

    # extraction of name files "Normal" etc
    files = {}
    for i in range(len(hp.DIAGNOSTIC)):
        files[hp.DIAGNOSTIC[i]] = list()

    #Multi-class (Tue) or Multi-label (False)
    #option = True
    option = False

    print(files)
    print("taille total: ", len(detail_of_files.iloc[:, 0]))
    normal = []
    malade = []
    for i in range(len(detail_of_files.iloc[:, 0])):
        if option and len(detail_of_files.iloc[i, 6]) == 1:
            if len(files[detail_of_files.iloc[i,6][0]]) == 0:
              files[detail_of_files.iloc[i,6][0]] = [detail_of_files.iloc[i, 0]]
            else:
              files[detail_of_files.iloc[i,6][0]].append(detail_of_files.iloc[i,0])

            #Code STEP1
            """
            if 'Normal' in detail_of_files.iloc[i, 6]:
                normal.append(detail_of_files.iloc[i, 0])
            else:
                malade.append(detail_of_files.iloc[i, 0])
            """

        elif not option:
            for j in range(len(detail_of_files.iloc[i, 6])):
                if len(files[detail_of_files.iloc[i, 6][j]]) == 0:
                    files[detail_of_files.iloc[i, 6][j]] = [detail_of_files.iloc[i, 0]]
                else:
                    files[detail_of_files.iloc[i, 6][j]].append(detail_of_files.iloc[i, 0])

    element, minimum = sample_malade_temoin(files)

    print("Step1")
    # Choix du sous-ensemble utilisé
    print('taille set',minimum)

    #Code to obtain lenght of subset (STEP1)
    """
    print("Normal ", len(normal), " Malade ", len(malade))
    element = sample(normal, k=min(len(normal), len(malade)))
    """

    #Code to obtain lenght of subset (STEP2)
    """
    print(len(element))
    element = element + sample(malade, k=min(len(normal), len(malade)))
    print(len(element),len(element)/2)
    print(element)
    """

    print("Step2")
    #Création de la dataframe
    dataset = data(element)

    print("Step3")
    #Check si les électrodes n'ont pas été mises à l'envers
    dataset_check = mirror_data(dataset)

    print("Step4")
    #Création du dictionnaire répertoriant l'amplitude et le nombre d'échantillons propre au fichier.
    complement = information(element, detail_of_files)
    directory = Path.cwd().parent.parent.joinpath('data', 'processed', 'informations.txt')
    json.dump(complement, open(directory, "w"))

    return dataset_check, complement
