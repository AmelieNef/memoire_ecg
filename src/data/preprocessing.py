from statistics import median
import numpy as np
from scipy.signal import find_peaks, peak_prominences, filtfilt, butter, firwin, kaiserord, lfilter
from src.visualization.ECG_visualization import *
import heartpy.filtering as hf
from scipy.fftpack import fft
import pandas as pd
import src.PARAMETERS as hp
from pathlib import *
from src.visualization.ECG_visualization import Obtention_ECG

def action_reverse(data,name, layers):
    """
    Function to make data positive
    Parameters
    ----------
    data : initial data (mV)
    name : patient name
    layers : layers to make positive

    Returns
    -------
    data modified
    """
    true_data = []
    for i in range(len(data[name][layers])):
        if data[name][layers][i] > 0:
            true_data.append(-data[name][layers][i])
        elif data[name][layers][i] < 0:
            true_data.append(abs(data[name][layers][i]))
        else:
            true_data.append(data[name][layers][i])
    data[name][layers] = true_data
    return data

def mirror_data(data):
    """
    Function to check if all data are positive
    Parameters
    ----------
    data : data dictionnary of all patient

    Returns
    -------
    Updating data dictionnary
    """
    for name in data:
        for layers in data[name]:
            if abs(min(data[name][layers][100:])) > abs(max(data[name][layers][100:])) and median(data[name][layers]) > 0:
                data = action_reverse(data, name, layers)
    return data



#Preprocessing 1 : Baseline wander correction
def preprocessing_butterworth(element, cutoff):
    """
    Function to flatten ECG
    Parameters
    ----------
    element : data from one layer of patient
    cutoff : threshold of low frequency band to remave

    Returns
    -------
    Updating data
    """
    b, a = hf.iirnotch(cutoff, Q=0.005, fs=len(element))
    return filtfilt(b, a, element)

#Preprocessing 2 : high frequency removed
def butter_highpass(cutoff, fs, order):
    """
    Function to extract parameters need it to remove high frequency
    Parameters
    ----------
    cutoff : threshold of max accepted frequency
    fs : number of data present
    order : order of frequency

    Returns
    -------

    """
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='high', analog=False)
    return b, a

def preprocessing_highpass(element, cutoff, fs, order):
    """
    Function to filter higher frequency from initial data
    Parameters
    ----------
    element : data from one layer of a patient
    cutoff : threshold of max accepting frequency
    fs : number of data present
    order : order of frequency

    Returns
    -------
    Updating data
    """
    # Create an order 3 lowpass butterworth filter.
    b, a = butter_highpass(cutoff, fs, order)
    y = filtfilt(b, a, element)
    return y

def preprocessing_step(element, information, raw):
    """
    Function to apply preprocessing on data
    Parameters
    ----------
    element : data dictionnary of all patient
    information : information of all patient
    raw : window size

    Returns
    -------
    Updating data
    """
    cutoff_1 = 0.125
    order = 7
    cutoff_2 = 105
    print("CUTOFF 1 ", cutoff_1)
    print('CUTOFF 2 ', cutoff_2)
    print('ORDER ', order)

    for name in element.keys():
        for layers in element[name]:
            before = element[name][layers]

            #If we apply only preprocessing 1
            #"""
            element[name][layers] = preprocessing_butterworth(before, cutoff_1)
            #"""

            #If we apply preprocessing 1 and 2
            """
            intermediaire_1 = preprocessing_butterworth(before, cutoff_1)
            element[name][layers] = intermediaire_1 - preprocessing_highpass(intermediaire_1, cutoff_2, len(element),
                                                                            order)
                                                                            
            """

            if raw == hp.TRUE_WINDOW_RAW:
                element[name][layers] = element[name][layers].tolist()

            #Show ECG
            """
            Obtention_ECG_without_peaks_detection(element[name][layers], before, name, layers, cutoff_1)
            #Obtention_ECG_without_peaks_detection_2(element[name][layers], intermediaire_1, before, name, layers, cutoff_1,
            #                                                                           cutoff_2, order)
            """

    return element




