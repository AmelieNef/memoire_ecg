from pathlib import *
import pandas as pd


# NOT WORK !
def take_information():
    """
    Function to show distribution of our metrics
    Returns
    -------
    /
    """
    path = Path.cwd().parent.parent.joinpath('data', 'resultats_groupe')
    list_name_csv = [f.name for f in path.rglob('*.csv')]
    big_data = []
    for name in list_name_csv:
        data = pd.read_csv(Path.cwd().parent.parent.joinpath('data', 'resultats_groupe', name), index_col=["Name", "F1",
                                                                                                           "Precision",
                                                                                                           "Recall",
                                                                                                           "Accuracy",
                                                                                                           "Sensitivity",
                                                                                                           "Specificity",
                                                                                                           "PPV", "NPV",
                                                                                                           "False_positif",
                                                                                                           "False_negative"])
        big_data.append(data)
    big_data = pd.concat(big_data)
    big_data['Type'] = pd.Series([list_name_csv[0], list_name_csv[0], list_name_csv[0], list_name_csv[0],
                                  list_name_csv[0], list_name_csv[0], list_name_csv[1], list_name_csv[1],
                                  list_name_csv[1],
                                  list_name_csv[1], list_name_csv[1], list_name_csv[1], list_name_csv[2],
                                  list_name_csv[2],
                                  list_name_csv[2], list_name_csv[2], list_name_csv[2], list_name_csv[2],
                                  list_name_csv[3],
                                  list_name_csv[3], list_name_csv[3], list_name_csv[3], list_name_csv[3],
                                  list_name_csv[3]])

    print(big_data)
    big_data.boxplot()
# take_information()
