from pathlib import Path
import matplotlib.pyplot as plt


def Obtention_ECG(data, peaks, ordonnee, standard_unit, key):
    """
    Function to underline spike founded
    Parameters
    ----------
    data : raw data
    peaks : spike index
    ordonnee : value raw of spike
    standard_unit : normalise value
    key : name of layer

    Returns
    -------
    /
    """

    place = 1
    # plt.subplot(24, 1, place)
    plt.plot(peaks, ordonnee, "x")
    plt.plot(data)
    plt.title("layers " + key, fontdict={'fontsize': 8, 'fontweight': 'medium'})
    axes = plt.gca()
    axes.xaxis.set_ticklabels([str((2500 * i / int(standard_unit)) - 10) for i in range(1, 10, 1)])
    plt.xlabel("Temps en secondes", fontsize=5)
    plt.ylabel("Amplitude en mV", fontsize=5)
    place += 2
    plt.show()


def Obtention_ECG_without_peaks_detection(data, before, name, layers, cutoff):
    """
    Function to see ECG after one treatment
    Parameters
    ----------
    data : data after treatment
    before : data before treatment
    name : patient name
    layers : layers name
    cutoff : value of cutoff frequency used

    Returns
    -------
    /
    """

    fig, (ax0, ax1) = plt.subplots(nrows=2, constrained_layout=True)

    ax0.plot(before)
    ax0.set_title(f'RAW ECG - {layers} lead of id patient {name}')
    ax0.grid(True)
    ax0.set_xlabel("Time (sec)")
    ax0.set_ylabel("Amplitude (mV)")

    ax1.plot(data)
    ax1.set_title(f'Baseline wunder on ECG with cutoff {cutoff}')
    ax1.grid(True)
    ax1.set_xlabel("Time (sec)")
    ax1.set_ylabel("Amplitude (mV)")

    directory = Path.cwd().parent.parent.joinpath('picture', f'mygraph{name}_{cutoff}_{layers}.png')
    plt.savefig(directory)
    plt.close()
    # plt.show()


def Obtention_ECG_without_peaks_detection_2(data, intermediaire, before, name, layers, cutoff, frequency, order):
    """
    Function to see ECG after two treatment
    Parameters
    ----------
    data : data after two treatment
    intermediaire : data after one treatment
    before : data before treatment
    name : patient name
    layers : layers name
    cutoff : cutoff frequency lowest used
    frequency : cutoff frequency highest used
    order : order of cutoff frequency highest used

    Returns
    -------
    /
    """
    fig, (ax0, ax1, ax2) = plt.subplots(nrows=3, constrained_layout=True)

    ax0.plot(before)
    ax0.set_title(f'RAW ECG - {layers} lead of id patient {name}')
    ax0.grid(True)
    ax0.set_xlabel("Time (sec)")
    ax0.set_ylabel("Amplitude (mV)")

    ax1.plot(intermediaire)
    ax1.set_title(f'Baseline wunder on ECG with cutoff {cutoff}')
    ax1.grid(True)
    ax1.set_xlabel("Time (sec)")
    ax1.set_ylabel("Amplitude (mV)")

    ax2.plot(data)
    ax2.set_title(f'Filter on ECG with frequency {frequency} Hz and order {order}')
    ax2.grid(True)
    ax2.set_xlabel("Time (sec)")
    ax2.set_ylabel("Amplitude (mV)")

    directory = Path.cwd().parent.parent.joinpath('picture', f'mygraph{cutoff}_{name}_{frequency}_{order}_{layers}.png')
    plt.savefig(directory)
    plt.close()
    # plt.show()


def Obtention_ECG_all_leads(data, name):
    """
    Function to obtain all lead in one plot
    Parameters
    ----------
    data : raw data
    name : patient name

    Returns
    -------
    /
    """
    fig, (ax0, ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9, ax10, ax11) = plt.subplots(nrows=12)

    layers = list(data.keys())

    ax0.plot(data[layers[0]])
    ax0.set_title(f'RAW ECG - 12 leads from id patient {name}')
    ax0.grid(True)

    ax1.plot(data[layers[1]])
    ax1.grid(True)

    ax2.plot(data[layers[2]])
    ax2.grid(True)

    ax3.plot(data[layers[3]])
    ax3.grid(True)

    ax4.plot(data[layers[4]])
    ax4.grid(True)

    ax5.plot(data[layers[5]])
    ax5.grid(True)
    ax5.set_ylabel("Amplitude (mV)")

    ax6.plot(data[layers[6]])
    ax6.grid(True)

    ax7.plot(data[layers[7]])
    ax7.grid(True)

    ax8.plot(data[layers[8]])
    ax8.grid(True)

    ax9.plot(data[layers[9]])
    ax9.grid(True)

    ax10.plot(data[layers[10]])
    ax10.grid(True)

    ax11.plot(data[layers[11]])
    ax11.grid(True)
    ax11.set_xlabel("Time (sec)")

    plt.subplots_adjust(left=0.1, bottom=0.1, right=0.95, top=0.9, wspace=0.20, hspace=0.20)
    directory = Path.cwd().parent.parent.joinpath('picture', f'mygraph{name}_all_layers.png')
    plt.savefig(directory)
    plt.close()
    plt.show()
