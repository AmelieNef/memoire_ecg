from pathlib import *
import pandas as pd
from datetime import *
import matplotlib.pyplot as plt
import src.PARAMETERS as hp
import numpy as np
from itertools import cycle
import seaborn as sns


def getback_csv(file_name, A):
    """
    Function to get back csv file
    Parameters
    ----------
    file_name : patient name
    A : index indicator of exception

    Returns
    -------
    files data extract
    """
    path = Path.cwd().parent.parent.joinpath('data', 'modification_graph')
    solution_list = [f.name for f in path.rglob(file_name)]
    solution = pd.read_csv(Path.cwd().parent.parent.joinpath('data', 'modification_graph', solution_list[0]))
    if A == 0:
        solution = solution.set_index("Unnamed: 0")
    print(solution)
    return solution


def plot_violin(dataframe, begin, end, A):
    """
    Function to obtain violin plot
    Parameters
    ----------
    dataframe : dataframe of data to plot
    begin : step init
    end : step ending
    A : index indicator

    Returns
    -------
    /
    """
    df = dataframe.melt(var_name="STEP", value_name="ACCURACY")
    df["ACCURACY"] = [float(df["ACCURACY"][i].replace(',', '.')) for i in range(len(df["ACCURACY"]))]
    ax = sns.violinplot(data=df, x="STEP", y="ACCURACY", palette="inferno")
    if A == 0:
        plt.title(f'All accuracy values obtained from step {begin} to step {end} with RAW data and RR features')
    elif A == 1:
        plt.title(f'Accuracy values obtained from step {begin} to step {end}')
    else:
        plt.grid()
        plt.title(f'Accuracy values obtained to diagnose {begin}')
    # timestamp = datetime.today().strftime('%Y%m%d%H%M%S')
    # directory = Path.cwd().parent.parent.joinpath('picture', f'mygraph{timestamp}.png')
    # plt.savefig(directory)
    plt.show()


def plot_histo_curve(dataframe, begin, end, A):
    """
    Function to plot histogramme
    Parameters
    ----------
    dataframe : data to plot
    begin : begin step
    end : ending step
    A : index indicator

    Returns
    -------
    /
    """
    i = 0
    name = ["baseline wander filter with RAW data", "baseline wander filter with features data",
            "baseline wander filter and high frequency filter with RAW data",
            "baseline wander filter and high frequency filter  with features data"]

    for column in dataframe.columns:
        dataframe[column] = [float(dataframe[column][i].replace(',', '.')) for i in range(len(dataframe[column]))]
        df = pd.DataFrame(dataframe[column])
        df.columns = ["ACCURACY"]
        df["METHOD"] = ["SVM", "DT", "KNN", "RF", "LR", "GNB", "AdaBoost",
                        "IRIDIA", "LANCET", "LSTM"]
        ax = sns.barplot(data=df, x="METHOD", y="ACCURACY", palette="inferno")
        plt.title(f'All accuracy values obtained from different machine learning process with {name[i]}')
        i += 1
        # timestamp = datetime.today().strftime('%Y%m%d%H%M%S')
        # directory = Path.cwd().parent.parent.joinpath('picture', f'mygraph{timestamp}.png')
        # plt.savefig(directory)
        plt.show()


def main():
    """
    Function to obtain a violin plot between each pairwise step
    Returns
    -------
    /
    """
    files_list = ["*step12.*", "*step12RF*", "*step23.*", "*step23RF*", "*step34.*", "*step34RF*", "*step35.*",
                  "*step35RF*", "*step46.*", "*step46RF*"]
    begin = ["1", "1", "2", "2", "3", "3", "3", "3", "4", "4"]
    end = ["2", "2", "3", "3", "4", "4", "5", "5", "6", "6"]
    for i in range(len(files_list)):
        A = i % 2
        develop = getback_csv(files_list[i], A)
        plot_violin(develop, begin[i], end[i], A)


def main_2():
    """
    Function to obtain a violin plot of each diagnose possible
    Returns
    -------
    /
    """
    files_list = ["*STE*", "*STD*", "*RBBB*", "*PAC*", "*PVC*", "*Normal*", "*LBBB*", "*IAVB*", "*AF*"]
    name = ["STE", "STD", "RBBB", "PAC", "PVC", "Normal", "LBBB", "I_AVB", "AF"]
    for i in range(len(files_list)):
        develop = getback_csv(files_list[i], 2)
        plot_violin(develop, name[i], 0, 2)


def main_3():
    """
    Function to obtain a histogram of all diagnose value
    Returns
    -------
    /
    """
    files_list = ["*step7*"]
    name = ["STE", "STD", "RBBB", "PAC", "PVC", "Normal", "LBBB", "I_AVB", "AF"]
    for i in range(len(files_list)):
        develop = getback_csv(files_list[i], 2)
        plot_histo_curve(develop, name[i], 0, 2)


main_3()
