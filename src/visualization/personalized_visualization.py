from pathlib import *
import pandas as pd
from datetime import *
import matplotlib.pyplot as plt
import src.PARAMETERS as hp
import numpy as np
from itertools import cycle


def getback_csv(file_name):
    """
    Function to get back data to create plot
    Parameters
    ----------
    file_name : patient name

    Returns
    -------
    data to make plot
    """
    path = Path.cwd().parent.parent.joinpath('data', 'modification_graph')
    solution_list = [f.name for f in path.rglob(file_name)]
    solution = pd.read_csv(Path.cwd().parent.parent.joinpath('data', 'modification_graph', solution_list[0]))
    solution.columns = [i for i in range(len(solution.columns))]
    solution = solution.set_index(0).T.to_dict('list')
    return solution


def reduce_number_auc(data):
    """
    Function to normalise AUC value to have only three number after ","
    Parameters
    ----------
    data : auc score

    Returns
    -------
    normalize data
    """
    for key in data.keys():
        data[key] = round(data[key][0], 3)
    print(data)
    return data


def plot_roc_curve(fpr, tpr, roc_auc):
    """
    Function to plot ROC curve
    Parameters
    ----------
    fpr : FPR value
    tpr : TPR value
    roc_auc : ROC AUC value

    Returns
    -------
    /
    """
    # First aggregate all false positive rates
    all_fpr = np.unique(np.concatenate([fpr[diagnostic] for diagnostic in hp.DIAGNOSTIC]))

    # Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for diagnostic in hp.DIAGNOSTIC:
        mean_tpr += np.interp(all_fpr, fpr[diagnostic], tpr[diagnostic])

    # Plot all ROC curves
    colors = cycle(['firebrick', 'crimson', 'indigo', 'deeppink', 'darkorange',
                    'violet', 'lightpink', 'goldenrod', 'bisque'])
    for i, color in zip(range(len(hp.DIAGNOSTIC)), colors):
        plt.plot(fpr[hp.DIAGNOSTIC[i]], tpr[hp.DIAGNOSTIC[i]], color=color, lw=2,
                 label=f'ROC Curve of {hp.DIAGNOSTIC[i]}')

    plt.plot([0, 1], [0, 1], 'k--', lw=2)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('FPR')
    plt.ylabel('TPR')
    plt.title('ROC Curves')
    # plt.legend(loc="lower right", fontsize=20)
    timestamp = datetime.today().strftime('%Y%m%d%H%M%S')
    directory = Path.cwd().parent.parent.joinpath('picture', f'mygraph{timestamp}.png')
    plt.savefig(directory)
    plt.show()


files_list = ["*fpr*", "*tpr*", "*auc*"]
fpr = getback_csv(files_list[0])
tpr = getback_csv(files_list[1])
auc = getback_csv(files_list[2])
auc = reduce_number_auc(auc)
plot_roc_curve(fpr, tpr, auc)
