import tensorflow as tf
import pickle
from datetime import datetime
import os
import sys
from src.metrics_files import *
from src.logger import Logger
from tensorflow.keras.callbacks import Callback
import time
from src.features.features_selection import *
from src.models.machine_learning import *


def build_model(raw):
    """
    Function to build IRIDIA model
    Parameters
    ----------
    raw: window size

    Returns
    -------
    building IRIDIA model
    """
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Conv1D(filters=100, kernel_size=3, input_shape=(raw, hp.NUMBER_OF_LAYERS)))
    model.add(tf.keras.layers.Conv1D(filters=100, kernel_size=3))
    model.add(tf.keras.layers.GlobalMaxPool1D())
    model.add(tf.keras.layers.Reshape((100, 1)))
    model.add(tf.keras.layers.Bidirectional(tf.keras.layers.GRU(units=100)))
    model.add(tf.keras.layers.Dense(units=len(hp.DIAGNOSTIC), activation="sigmoid", name='output'))
    o = tf.keras.optimizers.Adam(lr=hp.LEARNING_RATE)
    model.compile(optimizer=o, loss=hp.LOSS_FUNCTION)
    model.summary()
    return model


def sub_conv(index, model, kernel, stride, chanel):
    """
    Function to build some part of LANCET model
    Parameters
    ----------
    index : indicator of specific part to build it
    model : in progress building model
    kernel : kernel size need it
    stride : stride size need it
    chanel : chanel size need it

    Returns
    -------
    In progress building model updating
    """
    if index == 0:
        first = tf.keras.layers.Conv1D(kernel_size=kernel[1], filters=chanel, strides=stride[4])(model)
        second = tf.keras.layers.BatchNormalization()(first)
        third = tf.keras.layers.Activation('relu')(second)
        fourth = tf.keras.layers.Conv1D(kernel_size=kernel[1], filters=chanel, strides=stride[5])(third)
        fifth = tf.keras.layers.BatchNormalization()(fourth)
        sixth = tf.keras.layers.Activation('relu')(fifth)
        return sixth
    else:
        first = tf.keras.layers.Conv1D(kernel_size=kernel[1], filters=chanel, strides=stride[6])(model)
        second = tf.keras.layers.BatchNormalization()(first)
        return second


def CONV_block(model, kernel, stride, chanel):
    """
    Function to add CONV block to in progress building model
    Parameters
    ----------
    model : in progress building model
    kernel : kernel size need it
    stride : stride size need it
    chanel : chanel size need it

    Returns
    -------
    In progress building model updating
    """
    model_init_1 = sub_conv(0, model, kernel, stride, chanel)
    model_init_2 = sub_conv(1, model, kernel, stride, chanel)
    intermediate_model = tf.keras.layers.Concatenate(axis=1)([model_init_1, model_init_2])
    final_model = tf.keras.layers.Activation('relu')(intermediate_model)
    return final_model


def sub_iden(model, kernel, stride, chanel):
    """
    Function to build subpart of IDEN block
    Parameters
    ----------
    model : in progress building model
    kernel : kernel size need it
    stride : stride size need it
    chanel : chanel size need it

    Returns
    -------
    In progress building model updating
    """
    first = tf.keras.layers.Conv1D(kernel_size=kernel[2], filters=chanel, strides=stride[7])(model)
    second = tf.keras.layers.BatchNormalization()(first)
    third = tf.keras.layers.Activation('relu')(second)
    return third


def IDEN_block(model, kernel, stride, chanel):
    """
    Function to adding IDEN block to in progress building model
    Parameters
    ----------
    model : in progress building model
    kernel : kernel size need it
    stride : stride size need it
    chanel : chanel size need it

    Returns
    -------
    In progress building model updating
    """
    model_init_1 = sub_iden(model, kernel, stride, chanel)
    intermediate_model = tf.keras.layers.Concatenate(axis=1)([model_init_1, model])
    model_final = tf.keras.layers.Activation('relu')(intermediate_model)
    return model_final


def DENSE_block(model):
    """
    Function to add DENSE block of in progress building model
    Parameters
    ----------
    model : in progress building model

    Returns
    -------
    In progress building model updating
    """
    model_init = tf.keras.layers.Dense(units=512)(model)
    model_intermediate = tf.keras.layers.Activation('relu')(model_init)
    model_final = tf.keras.layers.Dropout(rate=0.6)(model_intermediate)
    return model_final


def build_model_lancet(raw):
    """
    Function to build LANCET model
    Parameters
    ----------
    raw : window size

    Returns
    -------
    /
    """
    if raw == hp.TRUE_WINDOW_RAW:
        kernel = [5, 15, 15]
        stride = [2, 2, 2, 2, 2, 1, 2, 1]
        pooling = [2, 2, 2]
    else:
        kernel = [2, 1, 1]
        stride = [2, 2, 1, 2, 1, 1, 1, 1]
        pooling = [2, 1, 2]

    model_input = tf.keras.layers.Input(shape=(raw, hp.NUMBER_OF_LAYERS))
    model = tf.keras.layers.Conv1D(kernel_size=kernel[0], filters=64, strides=stride[0])(model_input)
    model = tf.keras.layers.BatchNormalization()(model)
    model = tf.keras.layers.Activation('relu')(model)
    model = tf.keras.layers.MaxPool1D(strides=stride[1], pool_size=pooling[0])(model)
    chanel_ = [64, 128, 256, 512]
    for repeat in range(4):
        model = CONV_block(model, kernel, stride, chanel_[repeat])
        model = tf.keras.layers.MaxPool1D(strides=stride[2], pool_size=pooling[1])(model)
        model = IDEN_block(model, kernel, stride, chanel_[repeat])
    model = tf.keras.layers.AveragePooling1D(strides=stride[3], pool_size=pooling[2])(model)
    model = tf.keras.layers.Flatten()(model)
    for repeat in range(2):
        model = DENSE_block(model)
    output_layer = tf.keras.layers.Dense(units=len(hp.DIAGNOSTIC), activation="sigmoid", name='output')(model)
    model = tf.keras.models.Model(inputs=model_input, outputs=output_layer)
    o = tf.keras.optimizers.Adam(lr=hp.LEARNING_RATE)
    model.compile(optimizer=o, loss=hp.LOSS_FUNCTION)
    model.summary()
    return model


def build_RNN_model(raw):
    """
    Function to build LSTM model
    Parameters
    ----------
    raw : window size

    Returns
    -------
    building model
    """
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Input(shape=(raw, hp.NUMBER_OF_LAYERS)))
    model.add(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(units=100)))
    model.add(tf.keras.layers.Reshape((200, 1)))
    model.add(tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(units=100)))
    model.add(tf.keras.layers.Dropout(0.5))
    model.add(tf.keras.layers.Dense(units=len(hp.DIAGNOSTIC), activation="softmax", name='output'))
    o = tf.keras.optimizers.Adam(lr=hp.LEARNING_RATE)
    model.compile(optimizer=o, loss=hp.LOSS_FUNCTION)
    model.summary()
    return model


def get_callbacks():
    """
    Function to obtain callback to send model before training
    Returns
    -------
    parameters to early stopping training model
    """
    es = tf.keras.callbacks.EarlyStopping(monitor='val_loss',
                                          patience=hp.PATIENCE, verbose=1, mode='min', restore_best_weights=True)
    histories = Histories()
    return [es, histories], histories


class Histories(Callback):
    def __init__(self):
        super().__init__()
        self.accuracies = []
        self.losses = []
        self.f1 = []

    def on_batch_end(self, batch, logs=None):
        if logs is None:
            logs = {}
        self.losses.append(logs.get('loss'))
        self.accuracies.append(logs.get('acc'))
        self.f1.append(logs.get('f1'))


def train(train_x, train_y, val_x, val_y, raw):
    """
    Function to construct and train model
    Parameters
    ----------
    train_x : data train
    train_y : label train
    val_x : data validation
    val_y : label data
    raw : window size

    Returns
    -------
    train model
    """
    model_name = 'Cedric_model'
    model = build_model(raw)
    # model_name = 'Lancet_model'
    # model = build_model_lancet(raw)
    # model_name = 'RNN_model'
    # model = build_RNN_model(raw)
    print(model.input_shape)
    print(model.output_shape)
    print(model_name)
    callbacks, histories = get_callbacks()

    model.fit(x=train_x, y=train_y, epochs=hp.EPOCH, batch_size=hp.BATCH_SIZE, verbose=1,
              validation_data=(val_x, val_y), shuffle=True, callbacks=callbacks)
    return model, model_name


def test(model, x_test, y_test, timestamp, name, model_name, raw):
    """
    Function to test training model
    Parameters
    ----------
    model : training model
    x_test : data test
    y_test : label test
    timestamp : actual time
    name : patient name
    model_name : model name
    raw : window size

    Returns
    -------
    AUC of test model
    """

    y_pred = model.predict(x_test)
    print('test part', len(x_test), len(y_pred))

    directory = Path.cwd().parent.parent.joinpath('data', 'interim', f'w{raw}_d{hp.DISTANCE}',
                                                  f'{timestamp}_pred_test_d{hp.DISTANCE}_w{raw}')
    with open(directory, "wb") as f:
        a = [y_test, y_pred]
        pickle.dump(a, f)

    auc = metrics_formula(y_test, y_pred, name, model_name)
    return auc


def start_log(raw):
    """
    Function to saved terminal
    Parameters
    ----------
    raw : window size

    Returns
    -------
    actual time
    """
    timestamp = datetime.today().strftime('%Y%m%d%H%M%S')
    directory = Path.cwd().parent.parent.joinpath('data', 'interim', f'w{raw}_d{hp.DISTANCE}')
    if not os.path.exists(directory):
        os.makedirs(directory)

    log_file = Path.cwd().parent.parent.joinpath('data', 'interim', f'w{raw}_d{hp.DISTANCE}', f'{timestamp}.log')
    sys.stdout = Logger(log_file)

    print(f"timestamp {timestamp}")
    print(f"log_file{log_file}")
    print(f"window {raw}, distance {hp.DISTANCE}, tolerance {hp.TOLERANCE}")
    print(f"epoch {hp.EPOCH}, patience {hp.PATIENCE}, lr {hp.LEARNING_RATE}")

    return timestamp


def subreshape(data_list, type_list, raw):
    """
    Function to adapt data as we want
    Parameters
    ----------
    data_list : list of data
    type_list : subdataset name
    raw : window size

    Returns
    -------
    reshape data
    """
    new_list = []
    for patient in range(len(data_list[type_list])):
        new_scale = []
        for data in range(len(data_list[type_list][patient][0])):
            new_subscale = []
            for layers in range(len(data_list[type_list][patient])):
                new_subscale.append(data_list[type_list][patient][layers][data])
            new_scale.append(new_subscale)
        new_list.append(new_scale)
    new_list = np.reshape(new_list, (len(new_list), raw, hp.NUMBER_OF_LAYERS))
    return new_list


def reshape_data(data_list, raw):
    """
    Function to shape correctly data before to be send to model
    Parameters
    ----------
    data_list : data to adapt
    raw : window size

    Returns
    -------
    adapted data
    """
    x_train = subreshape(data_list, 0, raw)
    x_validation = subreshape(data_list, 1, raw)
    x_test = subreshape(data_list, 2, raw)
    return x_train, x_validation, x_test


def predict_model(raw):
    """
    Function to start execution model NN or others
    Parameters
    ----------
    raw : window size

    Returns
    -------
    /
    """
    start_time = time.time()

    timestamp = start_log(raw)

    seed = np.random.randint(hp.MAX_SEED)
    print(f"Seed: {seed}")
    np.random.seed(seed)

    dataset_time = time.time()

    x_train, y_train, x_validation, y_validation, x_test, y_test, name, x, y = feature_select(raw)
    x_train, x_validation, x_test = reshape_data([x_train, x_validation, x_test], raw)
    dataset_time = time.time() - dataset_time
    # machine_learning(x_train, y_train, x_validation, y_validation, x_test, y_test, timestamp, raw)
    # """"
    model, model_name = train(x_train, y_train, x_validation, y_validation, raw)
    auc = test(model, x_test, y_test, timestamp, name, model_name, raw)

    model.save(Path.cwd().parent.parent.joinpath('data', 'interim', f"w{raw}_d{hp.DISTANCE}",
                                                 f"{timestamp}-model.h5"))

    run_time = time.time() - start_time
    print("--- %s seconds ---" % run_time)

    directory = Path.cwd().parent.parent.joinpath('data', 'interim', f"{timestamp}_result_values")
    with open(directory, "wb") as f:
        a = [auc, run_time, dataset_time]
        pickle.dump(a, f)
    # """


print('not raw')
predict_model(hp.TRUE_WINDOW)
