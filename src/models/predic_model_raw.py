from src.data.build_dataset_raw import *
from src.models.predict_model import *


def predict_model_raw(raw):
    """
    Function to Launch execution model NN or machine learning
    Parameters
    ----------
    raw : window size

    Returns
    -------
    /
    """
    start_time = time.time()

    timestamp = start_log(raw)

    seed = np.random.randint(hp.MAX_SEED)
    print(f"Seed: {seed}")
    np.random.seed(seed)

    dataset_time = time.time()

    x_train, y_train, x_validation, y_validation, x_test, y_test, name = build_dataset_raw(raw)
    x_train, x_validation, x_test = reshape_data(
        [x_train, x_validation, x_test], raw)
    dataset_time = time.time() - dataset_time

    machine_learning(x_train, y_train, x_validation, y_validation, x_test, y_test, timestamp)
    """"
    model, model_name = train(x_train, y_train, x_validation, y_validation, raw)
    auc = test(model, x_test, y_test, timestamp, name, model_name, raw)

    model.save(Path.cwd().parent.parent.joinpath('data', 'interim', f"w{raw}_d{hp.DISTANCE}",
                                                 f"{timestamp}-model.h5"))

    run_time = time.time() - start_time
    print("--- %s seconds ---" % run_time)

    directory = Path.cwd().parent.parent.joinpath('data', 'interim', f"{timestamp}_result_values")
    with open(directory, "wb") as f:
        a = [auc, run_time, dataset_time]
        pickle.dump(a, f)
    """


print("raw")
predict_model_raw(hp.TRUE_WINDOW_RAW)
