from pathlib import Path

from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import make_pipeline
from sklearn.tree import *
from sklearn.neighbors import *
from sklearn.ensemble import *
from sklearn.linear_model import *
from sklearn.model_selection import *
# linear discriminant functions
from sklearn.datasets import load_iris, make_multilabel_classification
from sklearn.multioutput import MultiOutputClassifier
from sklearn import svm, discriminant_analysis, naive_bayes
from sklearn import metrics
import numpy as np
import pandas as pd
import src.PARAMETERS as hp
from sklearn.preprocessing import StandardScaler


def reshape(a, b):
    """
    Function to reshape data to have a good dimension (5000,12) or (15,12)
    Parameters
    ----------
    a : data
    b : label

    Returns
    -------
    reshaped data
    """
    a = a.tolist()
    b = b.tolist()
    a = a + b
    return np.array(a)


# Support Vector Machines
def machine_learning(x_train, y_train, x_validation, y_validation, x_test, y_test, time, raw):
    """
    Function to apply a machine learning comparison
    Parameters
    ----------
    x_train : data train
    y_train : label train
    x_validation : data validation
    y_validation : label validation
    x_test : data test
    y_test : label test
    time : actual time
    raw : window size

    Returns
    -------
    /
    """
    print("machine")
    x_train = reshape(x_train, x_validation)
    y_train = reshape(y_train, y_validation)

    a = x_train.shape
    x_train = x_train.reshape(a[0], -1)

    b = x_test.shape
    x_test = x_test.reshape(b[0], -1)

    models = [svm.SVC, DecisionTreeClassifier, KNeighborsClassifier,
              RandomForestClassifier, LogisticRegression, naive_bayes.GaussianNB, AdaBoostClassifier]

    result = pd.DataFrame([["Accuracy"], ["Precision"], ["Recall"]], columns=["Model name"])
    for model in models:
        print("model ", model)
        clf = MultiOutputClassifier(model()).fit(x_train, y_train)
        y_pred = clf.predict(x_test)

        results = []
        results.append(np.mean(metrics.accuracy_score(y_test, y_pred)))
        results.append(np.mean(metrics.precision_score(y_test, y_pred, average=None)))
        results.append(metrics.recall_score(y_test, y_pred, average=None))

        result[str(model)] = results

        print(result)
        print("\n")

    if raw == hp.TRUE_WINDOW_RAW:
        directory = Path.cwd().parent.parent.joinpath('data', 'external', f"{time}_model_result_values_RAW.csv")
    else:
        directory = Path.cwd().parent.parent.joinpath('data', 'external',
                                                      f"{time}_model_result_values_RR_FEATURES.csv")
    result.to_csv(directory, index=False, header=True)
